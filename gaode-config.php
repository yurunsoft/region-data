<?php
return [
    // 设置高德 KEY
    'app_key'       =>  '',

    // 导出到 CSV
    'export_class'  =>  \Yurun\RegionData\GDExportCSVHandler::class,
    'csv'           =>  [
        'file'      =>  __DIR__ . '/export-' . time() . '.csv', // 保存文件名
        'delimiter' =>  ',',    // 字段分界符
        'enclosure' =>  '"',    // 字段环绕符
        'title'     =>  true,   // 包含标题
    ],

    // 导出到 MySQL
    // 'export_class'  =>  \Yurun\RegionData\GDExportMySQLHandler::class,
    'mysql'         =>  [
        'host'      =>  '127.0.0.1',
        'port'      =>  3306,
        'username'  =>  'root',
        'password'  =>  'root',
        'dbname'    =>  'db_tyiot',
    ],

    // 导出到 JSON
    // 'export_class'  =>  \Yurun\RegionData\GDExportJsonHandler::class,
    'json'         =>  [
        'listFileName'  => __DIR__ . '/list.json',
        'assocFileName' => __DIR__ . '/assoc.json',
        'childrenField' => 'children'
    ],
];