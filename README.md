# 高德行政区域数据格式化

## 介绍

使用高德地图行政区域 API 接口，支持将中国省市区街道数据导出为：CSV、MySQL、JSON。

宇润 PHP 交流群：17916227

## 使用说明

修改配置文件`gaode-config.php`

将想要导出的格式对应的 `export_class` 注释去除，修改相应配置

命令行运行：

```shell
php gaode.php
```

搞定~
